import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { BackgroundMode } from '@ionic-native/background-mode';
import { HomePage } from '../pages/home/home';
import { ServiceProvider } from '../providers/service/service';

import { Observable } from 'rxjs/Rx';

@Component({
  templateUrl: 'app.html',
  queries: {
    nav: new ViewChild('content')
  }
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;
  rootPage:any = HomePage;
  pages: Array<{ title: string, icon: string, count: number, component: any }>;
  temp: any;
  ticks: any;
  vetor: any =  Array();

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public serviceProvider: ServiceProvider,
    private backgroundMode: BackgroundMode) {
    this.initializeApp();
    this.serviceProvider.login();
    this.obervandoChamadas();
    this.backgroundMode.enable();
    this.pages = [
      {
        title: 'Home',
        icon: 'home',
        count: 0,
        component: HomePage
      },
      {
        title: 'Editar usuário',
        icon: 'people',
        count: 0,
        component: 'UpdatePage'
      }
    ];
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  obervandoChamadas() {
    let contatadorTempo = 0;
    let timer = Observable.timer(1, 1000);
    this.temp = timer.subscribe(
      t => {
        this.ticks = t;
        contatadorTempo++;
        if (contatadorTempo == 5) {
          this.serviceProvider.gerenciarChamadas()
          .then(data => {
            if(data){
              this.vetor = data;
              window.localStorage.setItem('tamanho', this.vetor.data.length);
              window.localStorage.setItem('dadosAtivado', JSON.stringify(data));
            }
          })
          .catch(erro => {
            console.log('ERRO: ', erro);
          })
          contatadorTempo = 0;
        }
      }
    )
  }

  openPage(page) {
    this.nav.setRoot(page.component);
  }
}

