import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { BackgroundMode } from '@ionic-native/background-mode';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ModalChamadasPage } from '../pages/modal-chamadas/modal-chamadas';
import { ChatProvider } from '../providers/chat/chat';
import { firebaseConfig } from '../providers/core-firebase/core-firebase';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabase } from 'angularfire2/database';
import { ChatSalaPage } from '../pages/chat-sala/chat-sala';
import { ServiceProvider } from '../providers/service/service';
import { Http, HttpModule } from '@angular/http';
import { AngularFireStorageModule } from 'angularfire2/storage';
import { FileChooser } from '@ionic-native/file-chooser';
import { File } from '@ionic-native/file';
import { Camera } from '@ionic-native/camera';
import { NotificacoesProvider } from '../providers/notificacoes/notificacoes';
import { LocalNotifications } from '@ionic-native/local-notifications';


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ChatSalaPage,
    ModalChamadasPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp, { tabsPlacement: 'top' }),
    AngularFireModule.initializeApp(firebaseConfig.fire),
    AngularFireStorageModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ChatSalaPage,
    ModalChamadasPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ChatProvider,
    AngularFireDatabase,
    ServiceProvider,
    BackgroundMode,
    Camera,
    FileChooser,
    File,
    NotificacoesProvider,
    LocalNotifications
  ]
})
export class AppModule {}
