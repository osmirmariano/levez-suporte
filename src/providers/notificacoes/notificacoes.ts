
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { LocalNotifications } from '@ionic-native/local-notifications';

@Injectable()
export class NotificacoesProvider {

  constructor(
    public http: Http,
    private localNotifications: LocalNotifications) {

  }

  notificacao() {
    console.log('CHEGOU AQUI MENSAGEM');
    this.localNotifications.schedule({
      id: 1,
      title: 'Nova mensagem',
      text: 'Você tem uma nova mensagem',
      smallIcon: "file://assets/imgs/icon.png",
      icon: "file://assets/imgs/icon.png",
      sound: "file://assets/sound/motoristaAtende.mp3"
    });
    this.localNotifications.cancelAll();
  }
}
