import { Injectable } from '@angular/core';
import { Config } from '../core/config.provider';
import { Headers,  RequestOptions, Http } from '@angular/http';
// import { HttpClientModule } from '@angular/common/http'; 
// import { HttpModule } from '@angular/http';

@Injectable()
export class ServiceProvider {
  private api: string = Config.config().host;
  public isLoggedin: boolean;
  public authToken;
  public refreshToken;
  dadosRetornoChamada: any;
  constructor(
    public http: Http) {
    
  }

  //Utiizada para obter token e atualizar
  armazenarCredeciaisUser(token, refresh_token) {
    window.localStorage.setItem('levez', token);
    window.localStorage.setItem('levez_refresh', refresh_token);
    this.credenciaisUser(token, refresh_token);
  }

  //Setando os dados da variáveis
  credenciaisUser(token, refresh_token) {
    this.isLoggedin = true;
    this.authToken = token;
    this.refreshToken = refresh_token;
  }

  //Fazer login
  login() {
    let dados = {
      username: 'adminbis365@levez.biz',
      password: 'qq160401',
      client_id: Config.config().client_id.toString(),
      client_secret: Config.config().secret,
      grant_type: Config.config().grant_type,
    }

    return new Promise((resolve, reject) => {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');
      headers.append('Accept', 'application/x-www-form-urlencoded');

      let options = new RequestOptions({
        headers: headers
      });

      this.http.post(this.api + '/api/v1/token', dados, options)
        .subscribe(res => {
          window.localStorage.setItem('token', res.json().access_token)
          this.armazenarCredeciaisUser(res.json().access_token, res.json().refresh_token);
          resolve(true);

        }, (err) => {
          reject(err);
        });
    });
  }

  gerenciarChamadas(){
    let token = window.localStorage.getItem('token');
    
    let headers = new Headers();
    headers.append('Authorization', 'Bearer ' + token);
    headers.append('Content-Type', 'application/json');

    let options = new RequestOptions({
      headers: headers
    });

    return new Promise((resolve, reject) => {
      this.http.get(this.api + '/api/v1/admin/chamada/monitor-chamadas', options)
        .subscribe(res => {
          resolve(res.json());
        }),
        err => {
          reject(err.json());
        }
    });
  }

  updateUsers(email){
    var token = localStorage.getItem('levez');
    let headers = new Headers();
    headers.append('Accept', 'application/json');
    headers.append('Authorization', 'Bearer ' + token);

    let options = new RequestOptions({
      headers: headers
    });
    let dados = {
      email: email
    }
    console.log('DADOS: ', dados);

    return new Promise((resolve, reject) => {
      // this.http.put(this.api+'/api/v1/front/user/userupdate', dados, options)
      this.http.put(this.api+'/api/v1/front/user/userupdate', dados, options)
        .subscribe(res => {
          console.log('CERTO: ', res.json());
          resolve(res.json());
        },
        (err) => {
          console.log('ERRO: ', err.json());
          reject(err);
      });
    });
  }
}
