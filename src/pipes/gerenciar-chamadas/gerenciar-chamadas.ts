import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'gerenciarChamadas',
})
export class GerenciarChamadasPipe implements PipeTransform {

  static dateDiff(a, b) {
    return a - +(b.valueOf());
  }

  transform(value: any, args?: any): any {
    return GerenciarChamadasPipe.dateDiff(Date.now(), new Date(value));
  }
}
