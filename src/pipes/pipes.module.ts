import { NgModule } from '@angular/core';
import { GerenciarChamadasPipe } from './gerenciar-chamadas/gerenciar-chamadas';
@NgModule({
	declarations: [GerenciarChamadasPipe],
	imports: [],
	exports: [GerenciarChamadasPipe]
})
export class PipesModule {}
