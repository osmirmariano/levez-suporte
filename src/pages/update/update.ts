import { ServiceProvider } from './../../providers/service/service';
import { Http } from '@angular/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import { FormBuilder, Validators } from '@angular/forms';
import { EmailValidator } from '../../validators/email';
import { Users } from '../../model/user';

@IonicPage()
@Component({
  selector: 'page-update',
  templateUrl: 'update.html',
})
export class UpdatePage {
  public editarForm;
  model: Users;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private formularioBuilder: FormBuilder,
    public serviceProvider: ServiceProvider,
    private toast: ToastController) {

      this.editarForm = formularioBuilder.group({
        email: ['', Validators.compose([Validators.required, EmailValidator.isValid])]
      });
  
      this.model = new Users();
      
  }

  ionViewDidLoad() {
   
  }

  updateUser(){
    this.serviceProvider.updateUsers(this.model.email)
      .then(data => {
        this.toast.create({
          message: 'Alteração realizada com sucesso.',
          position: 'bottom',
          cssClass: 'certo-update',
          duration: 7000
        }).present();
      })
      .catch((error: any) => {
        this.toast.create({
          message: 'Não foi possível alterar os dados.',
          position: 'bottom',
          cssClass: 'erro-update',
          duration: 7000
        }).present();
    })
  }
}
