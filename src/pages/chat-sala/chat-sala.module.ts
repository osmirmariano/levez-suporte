import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ChatSalaPage } from './chat-sala';

@NgModule({
  declarations: [
    ChatSalaPage,
  ],
  imports: [
    IonicPageModule.forChild(ChatSalaPage),
  ],
})
export class ChatSalaPageModule {}
