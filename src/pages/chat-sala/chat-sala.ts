import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ViewController } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage, AngularFireUploadTask } from 'angularfire2/storage';

import { Validators, FormBuilder } from '@angular/forms';
import { Camera, CameraOptions } from '@ionic-native/camera';


@IonicPage()
@Component({
  selector: 'page-chat-sala',
  templateUrl: 'chat-sala.html',
})
export class ChatSalaPage {
  @ViewChild(Content) content: Content;
  public chatForm;
  model: ChatUsers;
  task: AngularFireUploadTask;
  progress: any;  // 0 a 100
  image: string; // base64

  listaConversas = [];
  imagem = [];
  referencia;
  arquivo;
  foto;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public db: AngularFireDatabase,
    private formularioBuilder: FormBuilder,
    public viewCtrl: ViewController,
    private camera: Camera,
    public storage: AngularFireStorage) {
      
    this.chatForm = formularioBuilder.group({
      mensagem: ['', Validators.compose([Validators.required])]
    })
    this.model = new ChatUsers();

    this.referencia = storage.storage.ref();
  }
  
  ionViewDidEnter() {
    let dimensions = this.content.getContentDimensions();
    this.content.scrollTo(0, dimensions.contentHeight + 100, 100);
  }
  
  scrollMensagem() {
    let dimensions = this.content.getContentDimensions();
    this.content.scrollTo(0, dimensions.scrollHeight + 100, 100);
  }

  ionViewDidLoad() {
    // let dados;
    this.db.list('/sala/' + this.navParams.get('dados')).valueChanges().subscribe(res => {
      this.listaConversas = res;
      
      // res.forEach(res => {
      //   dados = res;
        // if(dados.imagem){
        //   let caminho = this.referencia.child(dados.imagem);
        //   caminho.getDownloadURL().then(url => {
        //     this.imagem = url;
        //     console.log('IMNAGEMGMMEMM', url); 
        //   });
        // }
      // })
    });
  }


  chatSend() {
    var data = this.dataHoje();
    this.db.list('/sala/' + this.navParams.get('dados')).push({
      name: 'Suporte Levez',
      mensagem: this.model.mensagem,
      data: data
    });
    this.model.mensagem = '';
    this.scrollMensagem();
  }

  dataHoje() {
    var data = new Date();
    var dia = data.getDate();
    var mes = data.getMonth() + 1;
    var ano = data.getFullYear();
    var horas = new Date().getHours();
    var minutos = new Date().getMinutes();
    var segundos = new Date().getSeconds();
    var resultado = dia + "/" + mes + "/" + ano + " - " + horas + "h:" + minutos + "min:" + segundos + "s";
    return resultado;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  async captureImage() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY
    }
    return await this.camera.getPicture(options)
  }

  createUploadTask(file: string): void {
    const filePath = `img-suporte-levez_${ new Date().getTime() }.jpg`;
    this.image = 'data:image/jpg;base64,' + file;
    this.task = this.storage.ref(filePath).putString(this.image, 'data_url');
    this.progress = this.task.percentageChanges();
    var data = this.dataHoje();
    
    this.db.list('/sala/' + this.navParams.get('dados')).push({
      name: 'Suporte Levez',
      imagem: filePath,
      data: data,
    });  
  }
  
  async salvarImagem() {
    const base64 = await this.captureImage();
    this.createUploadTask(base64);
  }
}

export class ChatUsers {
  mensagem: string;
}
