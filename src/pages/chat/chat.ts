import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { ChatProvider } from '../../providers/chat/chat';
// import firebase from 'firebase/app';
import { ChatSalaPage } from '../chat-sala/chat-sala';
import { ModalController } from 'ionic-angular';
import { AngularFireStorage } from 'angularfire2/storage';
import { NotificacoesProvider } from '../../providers/notificacoes/notificacoes';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  listaChat: any;
  listaConversas = [];
  idUsuarioChat = [];
  idTratamento: any = 0;
  referencia: any;
  vazio: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public db: AngularFireDatabase,
    public chatProvider: ChatProvider,
    private modalCtrl: ModalController,
    private storage: AngularFireStorage,
    public notificacoesProvider:  NotificacoesProvider) {
      this.referencia = storage.storage.ref();
  }

  ionViewDidLoad() {
    this.db.list('/sala/').snapshotChanges().subscribe(res => {
      let resultado = res;
      console.log('OOOOO: ', this.navCtrl.getActive().name);
      if(this.navCtrl.getActive().component != 'ChatSalaPage'){
        this.notificacoesProvider.notificacao();
      }
      
      console.log(res.length);
      if(res.length == 0){
        this.vazio = 1;  
      }
      else{
        this.vazio = 2;
      }
      this.listaConversas = [];
      resultado.map(item => {
        this.db.list('/sala/'+item.key).valueChanges().subscribe(resultado => {
          this.listaConversas.push(resultado[0]);
        });                                                                                                                                                                         
      });
    });
    
  }

 
  conversar(item){
    let modal = this.modalCtrl.create(ChatSalaPage, { dados: item.id });
    modal.present();
  }
}

export class ChatUsers {
  mensagem: string;
}