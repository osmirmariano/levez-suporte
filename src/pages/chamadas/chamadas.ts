import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { ServiceProvider } from '../../providers/service/service';
import { Observable } from 'rxjs/Rx';


import { ActionSheetController } from 'ionic-angular';
import { ModalChamadasPage } from '../modal-chamadas/modal-chamadas';

@IonicPage()
@Component({
  selector: 'page-chamadas',
  templateUrl: 'chamadas.html',
})
export class ChamadasPage {
  temp: any;
  tempoAtiva: any;
  ticks: any;
  ticks2: any;
  dadosAtivado: any = [];
  mudaCorCard: any;
  vazio: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public service: ServiceProvider,
    public actionSheetCtrl: ActionSheetController,
    private modalCtrl: ModalController) {   
    this.monitorChamadas(); 
  }

  ionViewDidLoad() {}

  monitorChamadas() {
    let contatadorTempo = 0;
    let timer = Observable.timer(1, 1000);
    
    this.temp = timer.subscribe(
      t => {
        this.ticks = t;
        contatadorTempo++;
        if (contatadorTempo == 5) {
          this.dadosAtivado = JSON.parse(window.localStorage.getItem('dadosAtivado')).data;
          contatadorTempo = 0;
          
          if(JSON.parse(window.localStorage.getItem('tamanho')) == 0){
            this.vazio = 1;  
          }
          else{
            this.vazio = 2;
          }
        }
      }
    )
  }

  mostrarDados(item){
    let modal = this.modalCtrl.create(ModalChamadasPage, { dados: item });
    modal.present();
  }
}
