import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { ChatSalaPage } from '../chat-sala/chat-sala';
import { ChamadasPage } from '../chamadas/chamadas';
import { ChatPage } from '../chat/chat';
import { ServiceProvider } from '../../providers/service/service';
import { Http } from '@angular/http';

@NgModule({
  declarations: [
    HomePage,
    ChatPage,
    ChamadasPage,
    ChatSalaPage,
    ServiceProvider
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
  ]
})
export class HomePageModule {}
