import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ModalChamadasPage } from './modal-chamadas';

@NgModule({
  declarations: [
    ModalChamadasPage,
  ],
  imports: [
    IonicPageModule.forChild(ModalChamadasPage),
  ],
})
export class ModalChamadasPageModule {}
