import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';


@IonicPage()
@Component({
  selector: 'page-modal-chamadas',
  templateUrl: 'modal-chamadas.html',
})
export class ModalChamadasPage {
  dados: any;
  enderecoOrigem: any;
  enderecoDestino: any;

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public viewCtrl: ViewController) {
    this.dados = this.navParams.get('dados');
    console.log('DADOS: ', this.dados);
    this.enderecoOrigem = this.dados.trajeto.data[0].endereco;
    this.enderecoDestino = this.dados.trajeto.data[1].endereco;
  }

  mostraDados(){
    console.log('DADOS DEU CERTO: ', this.dados);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
